package com.epam.repository;

import java.util.List;

public interface CrudRepository<T> {
    T findById(int id);

    List<T> findAll();

    void save(T item);

    void save(List<T> map);

    void delete(int id);
}
