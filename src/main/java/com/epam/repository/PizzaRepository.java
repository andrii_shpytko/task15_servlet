package com.epam.repository;

import com.epam.model.Pizza;

import java.util.ArrayList;
import java.util.List;

public class PizzaRepository implements CrudRepository<Pizza> {
    List<Pizza> pizzaList = new ArrayList<>();

    @Override
    public Pizza findById(int id) {
        return pizzaList.get(id);
    }

    @Override
    public List<Pizza> findAll() {
        return pizzaList;
    }

    @Override
    public void save(Pizza item) {
        pizzaList.add(item);
    }

    @Override
    public void save(List<Pizza> pizzas) {
        for (Pizza pizza : pizzas){
            save(pizza);
        }
    }

    @Override
    public void delete(int id) {
        pizzaList.remove(id);
    }
}
