package com.epam.repository;

import com.epam.model.Order;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class OrderRepository implements CrudRepository<Order> {
    private List<Order> orderList = new ArrayList<>();

    @Override
    public Order findById(int id) {
        return orderList.get(id);
    }

    @Override
    public List<Order> findAll() {
        return orderList;
    }

    @Override
    public void save(Order item) {
        orderList.add(item);
    }

    @Override
    public void save(List<Order> orders) {
        for (Order order : orders) {
            save(order);
        }
    }

    @Override
    public void delete(int id) {
        orderList.stream()
                .filter(order -> order.getId() != id)
                .collect(Collectors.toList());
    }
}
