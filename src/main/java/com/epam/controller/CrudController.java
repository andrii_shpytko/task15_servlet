package com.epam.controller;

import javax.servlet.http.HttpServletRequest;

public interface CrudController {
    String get(HttpServletRequest request);

    void post(HttpServletRequest request);

    void put(HttpServletRequest request);

    void delete(HttpServletRequest request);
}
