package com.epam.service;

import com.epam.model.Client;
import com.epam.model.Order;
import com.epam.model.Pizza;
import com.epam.repository.OrderRepository;
import com.google.gson.Gson;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class OrderService {
    private final OrderRepository orderRepository;
    private final Gson gson;

    public OrderService() {
        orderRepository = new OrderRepository();
        gson = new Gson();
        init();
    }

    private void init() {
        List<Order> orders = new ArrayList<>();
        orders.add(new Order(1, new Pizza("Техас", 210), new Client("Andre", "0677977349", "Svobody Ave, 24")));
        orders.add(new Order(2, new Pizza("Гавайська", 159), new Client("Petro", "0507977349", "Teatralna Str, 13")));
        orders.add(new Order(3, new Pizza("П'ять сирів", 151), new Client("Olha", "0687977349", "Teatralna Str, 28")));
        orders.add(new Order(4, new Pizza("Барбекю", 115), new Client("Ilona", "0677977352", "Kniazia Romana Str, 33")));
        orders.add(new Order(5, new Pizza("Папероні з томатом", 160), new Client("Ivanna", "0678955448", "Kniazia Romana Str, 14")));
        orderRepository.save(orders);
    }

    public void save(HttpServletRequest request) {
        StringBuilder stringBuilder = new StringBuilder();
        String line = null;
        try {
            BufferedReader bufferedReader = request.getReader();
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Order order = gson.fromJson(stringBuilder.toString(), Order.class);
        orderRepository.save(order);
    }

    public void delete(int id) {
        orderRepository.delete(id);
    }

    public String getOrder(int id) {
        return gson.toJson(orderRepository.findById(id));
    }

    public String getAllOrder(){
        List<Order> orders = orderRepository.findAll();
        return gson.toJson(orders);
    }
}
