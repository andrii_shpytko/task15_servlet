package com.epam.service;

import com.epam.model.Pizza;
import com.epam.repository.PizzaRepository;
import com.google.gson.Gson;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PizzaService {
    private final PizzaRepository pizzaRepository;
    private final Gson gson;

    public PizzaService() {
        pizzaRepository = new PizzaRepository();
        gson = new Gson();
        init();
    }

    private void init() {
        List<Pizza> pizzas = new ArrayList();
        pizzas.add(new Pizza("Техас", 210));
        pizzas.add(new Pizza("Гавайська", 159));
        pizzas.add(new Pizza("П'ять сирів", 151));
        pizzas.add(new Pizza("Барбекю", 115));
        pizzas.add(new Pizza("Папероні з томатом", 145));
        pizzas.add(new Pizza("Баварська", 160));
        pizzas.add(new Pizza("Овочева Феєрія", 120));
        pizzas.add(new Pizza("Карбонара", 230));
        pizzas.add(new Pizza("Тоскана", 160));
        pizzas.add(new Pizza("Мюнхенська DeLUX", 190));
        pizzaRepository.save(pizzas);
    }

    public void save(HttpServletRequest request) {
        StringBuilder stringBuilder = new StringBuilder();
        String line = null;
        try {
            BufferedReader bufferedReader = request.getReader();
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Pizza pizza = gson.fromJson(stringBuilder.toString(), Pizza.class);
        pizzaRepository.save(pizza);
    }

    public void delete(int id) {
        pizzaRepository.delete(id);
    }

    public String getPizza(int id) {
        return gson.toJson(pizzaRepository.findById(id));
    }

    public String getAllPizza() {
        List<Pizza> pizzas = pizzaRepository.findAll();
        return gson.toJson(pizzas);
    }
}
