package com.epam.model;

public class Order {
    private int id;
    private Pizza pizza;
    private Client client;

    public Order(int id, Pizza pizza, Client client) {
        this.id = id;
        this.pizza = pizza;
        this.client = client;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Pizza getPizza() {
        return pizza;
    }

    public void setPizza(Pizza pizza) {
        this.pizza = pizza;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
